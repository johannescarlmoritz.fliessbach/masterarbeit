import time

import cv2
import numpy as np
from Application import *
from Application import *
from Calibration import *
from OpenFiles import *
from OpenImage import *
from OpenImage import *
from OpenImage import *
from SpectralData import *
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib




def Create_LuminanceImage(Image_Path, Path_DarkFlat, name="LuminanceImage", method="Finlay", cmap=False, vmin=None,vmax=None,map="hot"):
    def WriteImage(Luminance, name):

        """"
        Input:
            name:
            method:[Finlayson, IEC,Ensemble, All]
        """

        #Zusätzlich berücksichtigter Korrekturfaktor
        Korrekturfaktor=1.1

        Luminance = Luminance.T[0]*Korrekturfaktor
        Luminance = Luminance.reshape(img[0].shape)
        Luminance = Luminance.astype(np.float32)


        # for i in range(Luminance.shape[0]):
        #     for y in range(Luminance.shape[1]):
        #         if Luminance[i][y]<0:
        #             Luminance[i][y]=0



        cv2.imwrite(name+".tif", Luminance)


        if cmap:
            #plt.imshow(Luminance, cmap=map, norm=mpl.colors.LogNorm(vmin=vmin,vmax=vmax,clip=True))
            plt.imshow(Luminance, cmap=map, norm=mpl.colors.LogNorm(vmin=vmin, vmax=vmax, clip=True))
            cbar= plt.colorbar()
            cbar.set_label("Leuchtdichte in cd/m²")
            #plt.savefig(name+'.png')
            plt.show()

        return Luminance

    print("Starte LuminanceImage")
    t0 = time.time()
    img = ProcessImage(Image_Path, Path_DarkFlat, exif=False, display=False)
    AnalogGain, ShuterSpeed = Get_EXIF(Image_Path)


    t1 = time.time()
    print("Time ReadImage:", t1 - t0, " Sekunden")

    R_Raw = img[0].reshape(1, img[0].size)[0]

    G_Raw = img[1].reshape(1, img[0].size)[0]

    B_Raw = img[2].reshape(1, img[0].size)[0]


    img_RGB = np.empty((3, img[0].size), dtype=np.float32)
    img_RGB = (np.vstack((R_Raw, G_Raw, B_Raw)).T)

    #Normierung der RGB-Werte
    exposure_correction= ShuterSpeed/500000
    print("Expsoure_Korrektur:",exposure_correction)
    img_RGB=img_RGB/exposure_correction

    t0 = time.time()

    if method == "Finlayson":
        print("Method: Finlayson")
        Luminance = Apply_Transformation_FinlaysonSpectral(img_RGB)
        image=WriteImage(Luminance, "./Output/" + name + "F")

    if method == "IEC":
        print("Method: IEC")
        Luminance = Apply_Transformation_IEC(img_RGB)
        image = WriteImage(Luminance, "./Output/" + name + "I")

    if method == "CChecker":
        print("Method: ColorChecker")
        Luminance = Apply_Transformatin_CChecker(img_RGB)
        image = WriteImage(Luminance, "./Output/" + name + "C")

    if method == "Ensemble":
        print("Method: Ensemble")
        Luminance = Apply_Transformation_Ensemble(img_RGB)
        image =WriteImage(Luminance, "./Output/" + name + "E")

    if method=="FinlaysonChecker":
        print("Method: FinlaysonChecker")
        Luminance = Apply_Transformation_Finlayson(img_RGB)
        image = WriteImage(Luminance, "./Output/" + name + "FC")

    if method == "All":
        print("Method: All")

        print("Method: Finlayson")
        Luminance = Apply_Transformation_FinlaysonSpectral(img_RGB)
        image =WriteImage(Luminance, "./Output/" + name + "FS")

        print("Method: IEC")
        Luminance = Apply_Transformation_IEC(img_RGB)
        image =WriteImage(Luminance, "./Output/" + name + "I")

        print("Method: ColorChecker")
        Luminance = Apply_Transformatin_CChecker(img_RGB)
        image =WriteImage(Luminance, "./Output/" + name + "C")

        print("Method: Ensemble")
        Luminance = Apply_Transformation_Ensemble(img_RGB)
        image =WriteImage(Luminance, "./Output/" + name + "E")

        print("Method: Finlay Checker")
        Luminance = Apply_Transformation_Finlayson(img_RGB)
        image = WriteImage(Luminance, "./Output/" + name + "FC")

    t1 = time.time()
    print("Time LuminanceImage:", t1 - t0, " Sekunden")




    return image



if __name__ == '__main__':
    Image = "D:/OneDrive - TU Darmstadt Fachgebiet Lichttechnik/Studienarbeit/Messungen/Messung 23.09/Skript/500000/Me3__500000_ag1.jpg"
    Path_DarkFlat = "C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Input/masterdark.jpg"

    Create_LuminanceImage(Image, Path_DarkFlat, name="Testauswertung", method="IEC", cmap=False, vmin=0,vmax=2,map="hot")


