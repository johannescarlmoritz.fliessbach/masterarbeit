import cv2
import matplotlib.pyplot as plt
import numpy as np
from OpenImage import *
from tqdm import tqdm
from Main import *
from LuminanceImage import *
# colour.plotting.colour_style()
import tqdm


def HDR(Image,Flat):

    Path_Dark = "F:/Von Desktop/Feldmessung/darkmerck/"
    Path_Image = "D:/Feldmessung/Pi/infrarot_vorfeld/"


    Summe = np.zeros((1520, 2028))
    Summe= Summe.astype(np.float32)
    Faktor = np.empty((1520, 2028))
    Vergleich = np.zeros((1520, 2028))

    for i, Pfad_I in (enumerate(Image)):
        img = ProcessImage(Path_Image+Pfad_I,Path_Image+Pfad_I)
        Luminance=Create_LuminanceImage(Path_Image+Pfad_I, Path_Image+Pfad_I, method="Ensemble")
        AnalogGain, ShuterSpeed = Get_EXIF(Path_Image+Pfad_I)
        # img_R=img_R/ShuterSpeed*100


        for x in range(img[0].shape[1]):
            for y in range(img[0].shape[0]):
                if img[0][y][x] < 500 or img[0][y][x] > 65535 * 0.9:
                    None
                else:

                    #Summe[y][x] = (Summe[y][x] + img[0][y][x])*10000 / ShuterSpeed
                    Summe[y][x] =(Summe[y][x]+Luminance[y][x])
                    Faktor[y][x] = Faktor[y][x] + 1
                    #Vergleich[y][x] = img[0][y][x] *100/ ShuterSpeed


        # plt.imshow(Vergleich)
        # plt.show()
        # cv2.imwrite((str(i) + ".png"), Vergleich)

    for x in range(img[0].shape[1]):
        for y in range(img[0].shape[0]):
            if Summe[y][x] == 0:
                None
            else:
                if Summe[y][x] / Faktor[y][x]<100:
                    Summe[y][x] = Summe[y][x] / Faktor[y][x]
                else:
                    Summe[y][x]=0


    plt.imshow(Summe)
    plt.show()
    cv2.imwrite("Ensemble_Infrarot_0.tif", Summe)


if __name__ == '__main__':
    # Pfade_Image = ["_5meter_8000.jpeg", "_5meter_10000.jpeg", "_5meter_20000.jpeg", "_5meter_50000.jpeg",
    #                "_5meter_100000.jpeg", "_5meter_200000.jpeg", ]
    # Pfade_Flat = ["_dark_8000.jpeg", "_dark_10000.jpeg", "_dark_20000.jpeg", "_dark_50000.jpeg", "_dark_100000.jpeg",
    #               "_dark_200000.jpeg"]

    Pfade_Image = ["M2.1_206__(2021-01-12_0314_)_100000.jpg","M2.1_206__(2021-01-12_0314_)_200000.jpg","M2.1_206__(2021-01-12_0314_)_300000.jpg","M2.1_206__(2021-01-12_0314_)_400000.jpg","M2.1_206__(2021-01-12_0314_)_500000.jpg","M2.1_206__(2021-01-12_0314_)_600000.jpg","M2.1_206__(2021-01-12_0314_)_700000.jpg"]
    Pfade_Flat = ["darkmerck_1__(2021-01-12_0316_)_100000.jpg","darkmerck_1__(2021-01-12_0316_)_200000.jpg","darkmerck_1__(2021-01-12_0316_)_300000.jpg","darkmerck_1__(2021-01-12_0316_)_400000.jpg","darkmerck_1__(2021-01-12_0316_)_500000.jpg","darkmerck_1__(2021-01-12_0316_)_600000.jpg","darkmerck_1__(2021-01-12_0316_)_700000.jpg"]
    a = 0
    Pfade_Image=[f"infrarotinfrarot_vorfeld_{a}_100000.jpg",f"infrarotinfrarot_vorfeld_{a}_200000.jpg",f"infrarotinfrarot_vorfeld_{a}_300000.jpg",f"infrarotinfrarot_vorfeld_{a}_400000.jpg",f"infrarotinfrarot_vorfeld_{a}_500000.jpg",f"infrarotinfrarot_vorfeld_{a}_600000.jpg",f"infrarotinfrarot_vorfeld_{a}_700000.jpg",f"infrarotinfrarot_vorfeld_{a}_800000.jpg",f"infrarotinfrarot_vorfeld_{a}_900000.jpg",f"infrarotinfrarot_vorfeld_{a}_1000000.jpg",f"infrarotinfrarot_vorfeld_{a}_3000000.jpg"]
    #Pfade_Image=["infrarotinfrarot_vorfeld_0_100000.jpg","infrarotinfrarot_vorfeld_0_200000.jpg"]

    print("Startet", a)
    #Pfade_Image=[f"photopicphotopic_vorfeld_{a}_100000.jpg",f"photopicphotopic_vorfeld_{a}_200000.jpg",f"photopicphotopic_vorfeld_{a}_300000.jpg",f"photopicphotopic_vorfeld_{a}_400000.jpg",f"photopicphotopic_vorfeld_{a}_500000.jpg",f"photopicphotopic_vorfeld_{a}_600000.jpg",f"photopicphotopic_vorfeld_{a}_700000.jpg",f"photopicphotopic_vorfeld_{a}_800000.jpg",f"photopicphotopic_vorfeld_{a}_900000.jpg",f"photopicphotopic_vorfeld_{a}_100000.jpg",f"photopicphotopic_vorfeld_{a}_3000000.jpg"]
    HDR(Pfade_Image,Pfade_Image)
