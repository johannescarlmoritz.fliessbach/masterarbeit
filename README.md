**Überblick Skripte**

`Main.py`
- Main

`Application.py`
- Anwnedung Kalibrierung auf Bilder


`Calibration.py`
- Wendet Kalibrierung an

`CheckerScript.py`
- ColorChecker Detektion

`CorrectionFinlayson.py`
- Wurzelpolynom Farbkorrektur

`HDRImage.py`
- Erstellt High Dynamic Range Bild

`OpenFiles.py`
- Öffnet Excelfiles

`OpenImage.py`
- Öffnet RaspberryPi Raw-Image
- Postprocessing der Bilder
- Detektion ColorChecker

`SNR.py`
- Erstellt Signal to Noise Ratio

`SpectralData.py`
- Auswertung spektrale Daten Monochrommesstand

`Vignnetting.py`
- Vignettierungskorrektur

`BashImage.py`
- Bild aufnehmen RaspberryPi


**Implementierte Kalibrierverfahren**
- Linear mit ColorChecker
- Wurzelpolynom-Farbkorrektur mit ColorChecker
- Wurzelpolynom-Farbkorrektur mit spektralen Daten
- Farbtransformation nach IEC
- Ensemble (Kombinaton mehrerer Verfahren)