import warnings  # supress warnings

warnings.filterwarnings('ignore')

import xlrd

import numpy as np


def Auslesen_Leuchtdichte_Exel(Path):
    """
    Input: Pfad zu Exel-Tabelle
    Output: Leuchtdichte Testdaten in Array
    """

    book = xlrd.open_workbook(Path)
    first_sheet = book.sheet_by_index(0)

    rows = first_sheet.nrows - 1
    L_Test = np.empty((rows, 3))
    index = 0

    for i in range(1, first_sheet.nrows):
        L = first_sheet.cell(i, 0).value
        L_Test[index] = [L, L, L]
        index = index + 1

    return L_Test
