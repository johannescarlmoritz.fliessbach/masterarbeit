from __future__ import division, unicode_literals

import numpy as np
from colour.algebra import least_square_mapping_MoorePenrose
from colour.utilities import (CaseInsensitiveMapping, as_float_array, as_int,
                              closest, filter_kwargs, tsplit, tstack)
import scipy


def ExpansionFinlayson(RGB,
                       degree=1,
                       root_polynomial_expansion=True):

    R, G, B = tsplit(RGB)

    existing_degrees = np.array([1, 2, 3, 4])
    closest_degree = as_int(closest(existing_degrees, degree))
    if closest_degree != degree:
        raise ValueError('"Finlayson et al. (2015)" method does not define '
                         'a polynomial expansion for {0} degree, '
                         'closest polynomial expansion is {1} degree!'.format(
                             degree, closest_degree))

    if degree == 1:
        return RGB
    elif degree == 2:
        if root_polynomial_expansion:
            return tstack([
                R, G, B, (R * G) ** 1 / 2, (G * B) ** 1 / 2, (R * B) ** 1 / 2
            ])

        else:
            return tstack(
                [R, G, B, R ** 2, G ** 2, B ** 2, R * G, G * B, R * B])
    elif degree == 3:
        if root_polynomial_expansion:
            return tstack([
                R, G, B, (R * G) ** 1 / 2, (G * B) ** 1 / 2, (R * B) ** 1 / 2,
                (R * G ** 2) ** 1 / 3, (G * B ** 2) ** 1 / 3,
                (R * B ** 2) ** 1 / 3, (G * R ** 2) ** 1 / 3,
                (B * G ** 2) ** 1 / 3, (B * R ** 2) ** 1 / 3,
                (R * G * B) ** 1 / 3
            ])
        else:
            return tstack([
                R, G, B, R ** 2, G ** 2, B ** 2, R * G, G * B, R * B, R ** 3, G
                ** 3, B ** 3, R * G ** 2, G * B ** 2, R * B ** 2, G * R ** 2,
                B * G ** 2, B * R ** 2, R * G * B
            ])
    elif degree == 4:
        if root_polynomial_expansion:
            return tstack([
                R, G, B, (R * G) ** 1 / 2, (G * B) ** 1 / 2, (R * B) ** 1 / 2,
                (R * G ** 2) ** 1 / 3, (G * B ** 2) ** 1 / 3,
                (R * B ** 2) ** 1 / 3, (G * R ** 2) ** 1 / 3,
                (B * G ** 2) ** 1 / 3, (B * R ** 2) ** 1 / 3,
                (R * G * B) ** 1 / 3, (R ** 3 * G) ** 1 / 4,
                (R ** 3 * B) ** 1 / 4, (G ** 3 * R) ** 1 / 4,
                (G ** 3 * B) ** 1 / 4, (B ** 3 * R) ** 1 / 4,
                (B ** 3 * G) ** 1 / 4, (R ** 2 * G * B) ** 1 / 4,
                (G ** 2 * R * B) ** 1 / 4, (B ** 2 * R * G) ** 1 / 4
            ])
        else:
            return tstack([
                R, G, B, R ** 2, G ** 2, B ** 2, R * G, G * B, R * B, R ** 3, G
                ** 3, B ** 3, R * G ** 2, G * B ** 2, R * B ** 2, G * R ** 2,
                B * G ** 2, B * R ** 2, R * G * B, R ** 4, G ** 4, B ** 4,
                R ** 3 * G, R ** 3 * B, G ** 3 * R, G ** 3 * B, B ** 3 * R,
                B ** 3 * G, R ** 2 * G ** 2, G ** 2 * B ** 2, R ** 2 * B ** 2,
                R ** 2 * G * B, G ** 2 * R * B, B ** 2 * R * G
            ])


def CCMFinlayson(M_T,
                 M_R,
                 degree=1,
                 root_polynomial_expansion=True):

    CCM=least_square_mapping_MoorePenrose(
        ExpansionFinlayson(M_T, degree,
                           root_polynomial_expansion), M_R)
    CCM_Degree=degree
    scipy.io.savemat("./Data/CCMFDegree.mat", {"CCMFDegree": CCM_Degree})
    scipy.io.savemat("./Data/CCMFinlayson.mat", {"CCMFinlayson": CCM})
    return CCM




def ApplyFinlayson(RGB,root_polynomial_expansion=True):
    RGB = as_float_array(RGB)
    shape = RGB.shape

    RGB = np.reshape(RGB, (-1, 3))
    degree = scipy.io.loadmat("./Data/CCMFDegree.mat")["CCMFDegree"]

    RGB_e = ExpansionFinlayson(RGB, degree[0][0],
                               root_polynomial_expansion)
    CCM=scipy.io.loadmat("./Data/CCMFinlayson.mat")["CCMFinlayson"]

    return np.reshape(np.transpose(np.dot(CCM, np.transpose(RGB_e))), shape)



