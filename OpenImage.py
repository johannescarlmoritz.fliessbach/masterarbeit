
import io

import cv2
import imageio
import matplotlib.pyplot as plt
import numpy as np
import scipy
from PIL import Image
from PIL.ExifTags import TAGS
from colour_checker_detection import *
from scipy.io import loadmat
import CheckerScript
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib

def ReadRawPi(Path):
    """Input: Dateipfad"""

    """Output: img_R, img_G12, img_B 
            -> 16 bit Numpy-Arrays mit den Roten, Grünen bzw. Blauen Pixelwerten 
            -> Die Auflösung des Bildes viertelt sich aufgrund des Demosaicing auf (1520, 2028)"""

    img = Path
    # print("Image Name: ", img)


    file = open(img, 'rb')
    img = io.BytesIO(file.read())

    ver = 3
    offset = {
        3: 18711040,
    }[ver]

    data = img.getvalue()[-offset:]
    assert data[:4] == 'BRCM'.encode("ascii")

    data = data[32768:]
    data = np.frombuffer(data, dtype=np.uint8)

    reshape, crop = {
        3: ((3056, 6112), (3040, 6084)),
    }[ver]
    data = data.reshape(reshape)[:crop[0], :crop[1]]

    data = data.astype(np.uint16)
    shape = data.shape
    unpacked_data = np.zeros((shape[0], int(shape[1] / 3 * 2)), dtype=np.uint16)
    unpacked_data[:, ::2] = (data[:, ::3] << 4) + (data[:, 2::3] & 0x0F)
    unpacked_data[:, 1::2] = (data[:, 1::3] << 4) + ((data[:, 2::3] >> 4) & 0x0F)
    data = unpacked_data

    data[1::2, 0::2]  # RED
    data[0::2, 0::2]  # GREEN
    data[1::2, 1::2]  # GREEN
    data[0::2, 1::2]  # BLUE

    # Red
    img_R = data[0::2, 0::2] << 4
    #img_R = cv2.resize(img_R, (1014, 760), interpolation=cv2.INTER_LINEAR)
    #cv2.imwrite("R_mit.png", img_R)

    # Blue
    img_B = data[1::2, 1::2] << 4
    #cv2.imwrite("B_mit.png", img_B)
    #img_B = cv2.resize(img_R, (1014, 760), interpolation=cv2.INTER_LINEAR)

    # Green
    img_G1 = (data[1::2, 0::2] << 4)
    img_G2 = (data[0::2, 1::2] << 4)
    img_G12 = np.zeros((img_G1.shape[0], img_G2.shape[1]), dtype=np.uint16)


    for i in range(0, img_G1.shape[1]):
        for y in range(0, img_G1.shape[0]):
            img_G = (int(img_G1[y, i]) + int(img_G2[y, i])) / 2
            img_G12[y, i] = img_G
    #cv2.imwrite("G_mit.png", img_G12)
    #cv2.imwrite("F:/Von Desktop/DarkImages/"+str(nn)+".png", img_G12)
    #img_G12 = cv2.resize(img_R, (1014, 760), interpolation=cv2.INTER_LINEAR)

    print(np.mean(img_R),np.mean(img_G12),np.mean(img_B))
    return [img_R[0:1200][100:1700], img_G12[0:1200][100:1700], img_B[0:1200][100:1700]]


#[img_R[300:1200][200:1700], img_G12[300:1200][200:1700], img_B[300:1200][200:1700]]
#[img_R,img_G12,img_B]


"""Ausgabe der Exif-Informationen"""

def Get_EXIF(img):

    img = Image.open(img)
    exif_data = img._getexif()
    for tag, value in exif_data.items():
        #print(TAGS.get(tag, tag), value)
        tags = TAGS.get(tag, tag)

        if "MakerNote" in tags:
            # print(value)
            Makernote = str(value)
            AnalogGain = int(Makernote[Makernote.find("ag=") + 3:Makernote.find("focus")])
            ShutterSpeed = int(Makernote[Makernote.find("exp=") + 4:Makernote.find("ag=")])

            #print(AnalogGain)
            #print(ShuterSpeed)

    return AnalogGain,ShutterSpeed

def DisplayRGBImage(Image):

    imgR = (Image[0]/ 256).astype('uint8')
    imgG = (Image[1] / 256).astype('uint8')
    imgB = (Image[2] / 256).astype('uint8')

    img_RGB = cv2.merge((imgR, imgG, imgB))
    # plt.imshow(img_RGB)
    # plt.show()


    # cv2.imwrite("img_RGB.png", img_RGB)


def Apply_Correction_DarkCurent(Image,DarkFlat):


    # ImgR_c = Image[0] - ((DarkFlat[0]))
    # ImgG_c = Image[1] - ((DarkFlat[1]))
    # ImgB_c = Image[2] - ((DarkFlat[2]))
    ImgR_c = Image[0] - 4000
    ImgG_c = Image[1] - 4000
    ImgB_c = Image[2] - 4000

    #cv2.imwrite("img_GDark.png", ImgG_c[50:1200,800:1400])
    DisplayRGBImage([ImgR_c, ImgG_c, ImgB_c])
    return [ImgR_c,ImgG_c,ImgB_c]


def Apply_Correction_Vignetting(Image):
    LUT = LoadLUT()
    imgR_v = Image[0] * LUT[0]
    imgG_v = Image[1] * LUT[1]

    imgB_v = Image[2] * LUT[2]

    return [imgR_v, imgG_v, imgB_v]


def LoadLUT():

    LUT_R = scipy.io.loadmat("./Data/LUT_R.mat")["LUTR"]
    LUT_G = scipy.io.loadmat("./Data/LUT_G.mat")["LUTG"]
    LUT_B = scipy.io.loadmat("./Data/LUT_B.mat")["LUTB"]

    return [LUT_R, LUT_G, LUT_B]


def Segmentation_CChecker(Img_Raw, Image_Ref, display=True,loadimage=False,manual=False):

    try:

        if loadimage:
            image=Image_Ref
        else:
            image = cv2.imread(Image_Ref)


            image1=cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
            image = cv2.resize(image1, (1014, 760), interpolation=cv2.INTER_LINEAR)


        Img_Raw = [cv2.resize(Img_Raw[0], (1014, 760), interpolation=cv2.INTER_LINEAR),
                   cv2.resize(Img_Raw[1], (1014, 760), interpolation=cv2.INTER_LINEAR),
                   cv2.resize(Img_Raw[2], (1014, 760), interpolation=cv2.INTER_LINEAR)]


        for swatches, colour_checker, masks in CheckerScript.detect_colour_checkers_segmentation(image, additional_data=True):

            masks_i = np.zeros(colour_checker.shape)
            colour_checkers, clusters, swatches, image_c = CheckerScript.colour_checkers_coordinates_segmentation(image,
                                                                                                 additional_data=True)

            "Plot Contour Image"
            # plt.imshow(image_c)
            # plt.show()

            mask_be = np.empty((len(masks), 4))

            width_mean = int((colour_checkers[0][0][0] + colour_checkers[0][1][0]) / 2)
            hight_mean = int((colour_checkers[0][1][1] + colour_checkers[0][2][1]) / 2)



            """Falls die automatische ColorChecker Detektion nicht funktioniert, kann über width_mean und hight_mean die Position des Rasters verschoben werden"""
            if manual:
                width_mean = 380
                hight_mean=290

                # width_mean = 415
                # hight_mean = 305

                # width_mean = 370 / 375
                # hight_mean = 305 / 295

            for mask in range(len(masks)):
                mask_be[mask][0] = masks[mask][0] + hight_mean
                mask_be[mask][1] = masks[mask][1] + hight_mean
                mask_be[mask][2] = masks[mask][2] + width_mean
                mask_be[mask][3] = masks[mask][3] + width_mean

            # print(mask_be)

        image_resize = cv2.resize(image, (1014, 760), interpolation=cv2.INTER_LINEAR)

        masks_i = np.zeros(image_resize.shape)



        PixelColorChecker = np.zeros((24, 3))
        for i, mask in enumerate(masks):
            masks_i[mask[0] + hight_mean:mask[1] + hight_mean, mask[2] + width_mean:mask[3] + width_mean, ...] = 250

            #print("Rot:", i, "=",int(np.mean(Img_Raw[0][mask[0] + hight_mean:mask[1] + hight_mean, mask[2] + width_mean:mask[3] + width_mean])))

            PixelColorChecker[i][0] = int(
                np.mean(Img_Raw[0][mask[0] + hight_mean:mask[1] + hight_mean, mask[2] + width_mean:mask[3] + width_mean]))
            PixelColorChecker[i][1] = int(
                np.mean(Img_Raw[1][mask[0] + hight_mean:mask[1] + hight_mean, mask[2] + width_mean:mask[3] + width_mean]))
            PixelColorChecker[i][2] = int(
                np.mean(Img_Raw[2][mask[0] + hight_mean:mask[1] + hight_mean, mask[2] + width_mean:mask[3] + width_mean]))

            Img_Raw[0][mask[0] + hight_mean:mask[1] + hight_mean, mask[2] + width_mean:mask[3] + width_mean] = 1000
            Img_Raw[1][mask[0] + hight_mean:mask[1] + hight_mean, mask[2] + width_mean:mask[3] + width_mean] = 1000
            Img_Raw[2][mask[0] + hight_mean:mask[1] + hight_mean, mask[2] + width_mean:mask[3] + width_mean] = 1000
            image_resize[mask[0] + hight_mean:mask[1] + hight_mean, mask[2] + width_mean:mask[3] + width_mean] = 250

        # plt.imshow(masks_i)
        if display:
            plt.imshow(image_resize)
            plt.show()

    except ValueError:
        print("ColorChecker wurde nicht detektiert. Bitte helleres Referenzbild wählen")


    return PixelColorChecker



def ProcessImage(Image_Path,Dark_Path, exif=True,display=False, save=False):

    if exif:
        Get_EXIF(Image_Path)

    Dark = ReadRawPi(Dark_Path)
    #plt.imshow(Dark[0])
    #plt.show()
    print(np.mean(Dark))
    Img_Raw=ReadRawPi(Image_Path)
    #print("Dark",np.mean(Dark))



    Img_D=Apply_Correction_DarkCurent(Img_Raw,Dark)
    #Img_V=Apply_Correction_Vignetting(Img_D)


    if display:
        DisplayRGBImage(Img_D)

    if save:

        Image_RGB= np.empty((1520,2028,3), dtype="uint16")
        Image_RGB[:, :, 0] = Img_D[0]
        Image_RGB[:, :, 1] = Img_D[1]
        Image_RGB[:, :, 2] = Img_D[2]

        imageio.imsave('RGB.tiff', Image_RGB)

    return Img_D
#
# import rawpy
# def ReadSonyARW(path):
#     #Rawpy nicht kompatibel mit Phyton 3.8
#     #Python 3.7 funktioniert!
#
#
#     with rawpy.imread(path) as raw:
#         rgb = raw.postprocess(gamma=(1, 1), no_auto_bright=True, output_bps=16, )
#
#         raw_image = raw.raw_image.copy()
#         raw_colors=raw.raw_colors.copy()
#         black=raw.black_level_per_channel
#
#     Red=np.empty((1424,2144),dtype=np.float32)
#     Green = np.empty((1424,2144),dtype=np.float32)
#     Blue = np.empty((1424,2144),dtype=np.float32)
#
#     rowgerade=-1
#     rowungerade=-1
#     for i in range(raw_colors.shape[0]):
#         r = 0
#         g = 0
#         b = 0
#         if i %2:
#             #Gerade
#             rowgerade+=1
#         else:
#             rowungerade+=1
#
#         for y in range(raw_colors.shape[1]):
#             if raw_colors[i][y]==1:
#                 if y % 2:
#                     if raw_image[i][y]-black[1]<0:
#                         Green[rowgerade][g]=0
#                         g+=1
#                     else:
#                         Green[rowgerade][g]=raw_image[i][y]-black[1]
#                         g+=1
#
#
#                 else:
#                     None
#             elif raw_colors[i][y]==2:
#                 if raw_image[i][y]-black[2]<0:
#                     Blue[rowungerade][b]=0
#                     b += 1
#                 else:
#                     Blue[rowungerade][b]=raw_image[i][y]-black[2]
#                     b+=1
#             elif raw_colors[i][y]==0:
#                 if  raw_image[i][y]-black[0]<0:
#                     Red[rowgerade][r] = 0
#                     r += 1
#                 else:
#                     Red[rowgerade][r] = raw_image[i][y] - black[0]
#                     r+=1
#             else:
#                 print("fehler")
#
#     print(raw_image.shape)
#     plt.imshow(Green)
#     plt.show()
#
#     cv2.imwrite('rot.tif', Red)
#     cv2.imwrite('green.tif', Green)
#     cv2.imwrite('blue.tif', Blue)
#     cv2.imwrite("rgb.tif",rgb)
#
#     imageio.imsave('ro.png', Red)
#     imageio.imsave('green.png', Green)
#     imageio.imsave('blue.png', Blue)
#
#     return [Red,Green,Blue]
#
#


if __name__ == '__main__':
    Path_Image = "C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Input/Me1__500000_ag1.jpg"
    Path_DarkFlat = "C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Input/masterdark.jpg"

    PathARW1= "D:/OneDrive - TU Darmstadt Fachgebiet Lichttechnik/Studienarbeit/Sony Alpha/DSC00049.ARW"
    PathARW2 = "D:/OneDrive - TU Darmstadt Fachgebiet Lichttechnik/Studienarbeit/Sony Alpha/DSC00111.ARW"
    Path= "C:/Users/johan/Desktop/image.jpg"
    #Liest die RGB-Werte des Raw-Bildes und speichert sie in Numpy-Arrays
    #Img_Raw=ProcessImage(Path_Image,Path_DarkFlat,exif=True, display=True)

    Pathohne="F:/Messungen1.2/Streulichtblende/seiteohne/seiteohne_600000.jpg"
    Pathmit="F:/Messungen1.2/Streulichtblende/seitemit/seitemit_600000.jpg"

    Path="F:/SNRHome/2.Versuch/SNR_Dark/SNRDark_6000.jpg"
    #ReadRawPi(Pathohne)
    rgb=ReadRawPi(Pathmit)
    #print("lul")
    #print("ups")
    rgb=ReadSonyARW(PathARW1)

    print("lul")

    #PixelColorChecker=Segmentation_CChecker(Img_Raw,Path_Image,  display=True)

    # rgb= ReadSonyARW(PathARW2)

    #
    # PathARWREF= "C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Input/DSC00107.JPG"
    # RGB1= [rgb[:,:,0],rgb[:,:,1],rgb[:,:,2]]
    #
    #
    # print("CHeck")

    # Path= "F:/Von Desktop/DarkImages/_Rauschen_500000_"
    #
    # for i in range(1,17):
    #     print(i)
    #     ReadRawPi(Path+str(i)+".jpeg",i)