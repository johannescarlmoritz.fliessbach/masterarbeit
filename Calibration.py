from __future__ import division, unicode_literals

import warnings  # supress warnings

warnings.filterwarnings('ignore')

from scipy.io import loadmat
from SpectralData import *

warnings.filterwarnings('ignore')

from Calibration import *
from tqdm import tqdm
import colour
import luxpy as lx
import matplotlib.pyplot as plt
from colour import *
from sklearn.linear_model import LinearRegression
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import PolynomialFeatures
from sklearn import metrics
import pickle

import numpy as np
from colour.algebra import least_square_mapping_MoorePenrose
from colour.utilities import (as_float_array, as_int,
                              closest, tsplit, tstack)
import scipy
import OpenImage


def GetVlambda():
    sd_mesopic_luminous_efficiency_function = colour.PHOTOPIC_LEFS['CIE 1924 Photopic Standard Observer'].values
    sd_scotopic_luminous_efficiency_function = colour.SCOTOPIC_LEFS['CIE 1951 Scotopic Standard Observer'].values
    cmf = lx._CMF['1931_2']['bar']

    """Reduzierung auf Bereich 380-780 nm"""
    CMF = np.empty((400, 3))
    Vmes = np.empty((400, 3))
    Vsco = np.empty((400, 3))

    for i in range(20, 420):
        CMF[i - 20][0] = cmf[3][i]
        CMF[i - 20][1] = cmf[2][i]
        CMF[i - 20][2] = cmf[1][i]

        Vmes[i - 20] = sd_mesopic_luminous_efficiency_function[i - 20]
        Vsco[i - 20] = sd_scotopic_luminous_efficiency_function[i - 20]

    return Vmes, Vsco, CMF


###########################################################################################################################
####Calibration nach IEC: sRGB -> >Y (Lineare Transformation nach IEC), anschließend Y -> Luminanz (Lineare Regression)#####
############################################################################################################################

def Calibration_Transformation_IEC(RGB_T, L_T, degree_iec=1):
    """Determiniert den Zusammenhang zwischen den Y-Werten und der Leuchtdichte in Modell LinModel_IEC"""
    Y_CIE = IEC_Transformation(RGB_T)
    LinModel_IEC = GetLinModel(Y_CIE, L_T, save=True, savename="LinModel_IEC.pkl",degree=[degree_iec])


def IEC_Transformation(RGB):
    """Transformation nach IEC:1999"""

    CCM = np.array(
        [[0.2126729, 0.7151522, 0.0721750], [0.2126729, 0.7151522, 0.0721750], [0.2126729, 0.7151522, 0.0721750]])
    shape = RGB.shape
    return np.reshape(np.transpose(np.dot(CCM, np.transpose(RGB))), shape)


def IEC_Transformation2(RGB):
    """Transformation nach IEC:1999"""

    return colour.sRGB_to_XYZ(RGB)


###########################################################################################################################

def GetLinModel(Train, Train_L, degree=[1], save=False, savename="LinearMod\el.pkl"):
    # degree=[1, 2, 3, 4]
    """Lineare/Polynomiale Regression"""
    for i, degree in enumerate(degree):
        model = make_pipeline(PolynomialFeatures(degree), LinearRegression())
        model.fit(Train, Train_L)
        if save:
            savename = "./Data/" + savename
            with open(savename, 'wb') as file:
                pickle.dump(model, file)

    return model


#########################################################################################

def Calibration_Transformation_CChecker(RGB_T, L_T, degree_regression=1):
    GetLinModel(RGB_T, L_T, degree=[degree_regression], save=True, savename="LinModel_CChecker.pkl")


###########################################################################################################################
####Calibration Finlayson: Farbkorrektur nach Finlayson, anschließend Y -> Luminanz (Lineare/polynomiale Regression)#######
############################################################################################################################

import Application
def Calibration_Transformation_Finlayson_Spectral(RGB_Spectral, RGB_T, Luminance_T,degree_finlay=3, degree_regression=3):
    Vmes, Vsco, CMF = GetVlambda()
    CCM = CCMFinlaysonSpectral(RGB_Spectral, Vmes, degree=degree_finlay)
    Y_CIE = Apply_CCM_Finlayson(RGB_T, CCM, degree=degree_finlay)
    Model = GetLinModel(Y_CIE, Luminance_T, degree=[degree_regression], save=True, savename="ModelFinlaysonSpectral.pkl")

    # plot_RGB(RGB_Spectral,Vmes)
    return CCM, Model

def Calibration_Transformation_Finlayson(RGB_T, Luminance_T,degree_finlay=3, degree_regression=3):

    CCM = CCMFinlayson(RGB_T, Luminance_T, degree=degree_finlay)
    Y_CIE = Apply_CCM_Finlayson(RGB_T, CCM, degree=degree_finlay)
    Model = GetLinModel(Y_CIE, Luminance_T, degree=[degree_regression], save=True, savename="ModelFinlayson.pkl")

    # plot_RGB(RGB_Spectral,Vmes)
    return CCM, Model

def ExpansionFinlayson(RGB, degree=1, root_polynomial_expansion=True):
    R, G, B = tsplit(RGB)

    existing_degrees = np.array([1, 2, 3, 4])
    closest_degree = as_int(closest(existing_degrees, degree))
    if closest_degree != degree:
        raise ValueError("Finlayson - Fehler. Degree muss zwischen 1 und 4 liegen")

    if degree == 1:
        return RGB
    elif degree == 2:
        if root_polynomial_expansion:
            return tstack([
                R, G, B, (R * G) ** 1 / 2, (G * B) ** 1 / 2, (R * B) ** 1 / 2
            ])

        else:
            return tstack(
                [R, G, B, R ** 2, G ** 2, B ** 2, R * G, G * B, R * B])
    elif degree == 3:
        if root_polynomial_expansion:
            return tstack([
                R, G, B, (R * G) ** 1 / 2, (G * B) ** 1 / 2, (R * B) ** 1 / 2,
                         (R * G ** 2) ** 1 / 3, (G * B ** 2) ** 1 / 3,
                         (R * B ** 2) ** 1 / 3, (G * R ** 2) ** 1 / 3,
                         (B * G ** 2) ** 1 / 3, (B * R ** 2) ** 1 / 3,
                         (R * G * B) ** 1 / 3
            ])
        else:
            return tstack([
                R, G, B, R ** 2, G ** 2, B ** 2, R * G, G * B, R * B, R ** 3, G
                         ** 3, B ** 3, R * G ** 2, G * B ** 2, R * B ** 2, G * R ** 2,
                         B * G ** 2, B * R ** 2, R * G * B
            ])
    elif degree == 4:
        if root_polynomial_expansion:
            return tstack([
                R, G, B, (R * G) ** 1 / 2, (G * B) ** 1 / 2, (R * B) ** 1 / 2,
                         (R * G ** 2) ** 1 / 3, (G * B ** 2) ** 1 / 3,
                         (R * B ** 2) ** 1 / 3, (G * R ** 2) ** 1 / 3,
                         (B * G ** 2) ** 1 / 3, (B * R ** 2) ** 1 / 3,
                         (R * G * B) ** 1 / 3, (R ** 3 * G) ** 1 / 4,
                         (R ** 3 * B) ** 1 / 4, (G ** 3 * R) ** 1 / 4,
                         (G ** 3 * B) ** 1 / 4, (B ** 3 * R) ** 1 / 4,
                         (B ** 3 * G) ** 1 / 4, (R ** 2 * G * B) ** 1 / 4,
                         (G ** 2 * R * B) ** 1 / 4, (B ** 2 * R * G) ** 1 / 4
            ])
        else:
            return tstack([
                R, G, B, R ** 2, G ** 2, B ** 2, R * G, G * B, R * B, R ** 3, G
                         ** 3, B ** 3, R * G ** 2, G * B ** 2, R * B ** 2, G * R ** 2,
                         B * G ** 2, B * R ** 2, R * G * B, R ** 4, G ** 4, B ** 4,
                         R ** 3 * G, R ** 3 * B, G ** 3 * R, G ** 3 * B, B ** 3 * R,
                         B ** 3 * G, R ** 2 * G ** 2, G ** 2 * B ** 2, R ** 2 * B ** 2,
                         R ** 2 * G * B, G ** 2 * R * B, B ** 2 * R * G
            ])


def CCMFinlaysonSpectral(M_T, M_R, degree=1, root_polynomial_expansion=True):
    CCM = least_square_mapping_MoorePenrose(
        ExpansionFinlayson(M_T, degree,
                           root_polynomial_expansion), M_R)
    CCM_Degree = degree
    scipy.io.savemat("./Data/CCMFDegreeSpectral.mat", {"CCMFDegree": CCM_Degree})
    scipy.io.savemat("./Data/CCMFinlaysonSpectral.mat", {"CCMFinlayson": CCM})
    return CCM

def CCMFinlayson(M_T, M_R, degree=1, root_polynomial_expansion=True):
    CCM = least_square_mapping_MoorePenrose(
        ExpansionFinlayson(M_T, degree,
                           root_polynomial_expansion), M_R)
    CCM_Degree = degree
    scipy.io.savemat("./Data/CCMFDegree.mat", {"CCMFDegree": CCM_Degree})
    scipy.io.savemat("./Data/CCMFinlayson.mat", {"CCMFinlayson": CCM})
    return CCM


def Apply_CCM_Finlayson(RGB, CCM, degree=3):
    RGB = as_float_array(RGB)
    shape = RGB.shape

    RGB = np.reshape(RGB, (-1, 3))

    RGB_e = ExpansionFinlayson(RGB, degree)

    return np.reshape(np.transpose(np.dot(CCM, np.transpose(RGB_e))), shape)


##########################################################################################


def Cheung(Train, Train_L, Test, Test_L, degree=[1, 2, 3, 4]):
    for i, degree in enumerate(degree):
        values = (colour_correction(Test, Train, Train_L, method="Cheung 2004", degree=degree))

        # train = metrics.r2_score(Test_L, values)
        # print("Cheung {0}:  train score={1}".format(degree, train))

    return values


def Linear(Train, Train_L, Test, Test_L, degree=[1, 2, 3, 4], save=False, savename="LinearModel.pkl"):
    for i, degree in enumerate(degree):
        model = make_pipeline(PolynomialFeatures(degree), LinearRegression())
        model.fit(Train, Train_L)

        if save:
            savename = "./Data/" + savename
            with open(savename, 'wb') as file:
                pickle.dump(model, file)

        predict = model.predict(Test)

        train = metrics.r2_score(Test_L, predict)
        print("Polynomial Degree {0}:  train score={1}".format(degree, train))

    return predict


def plot():
    plt.legend(loc="upper right")
    plt.xlabel("Wellenlänge λ in nm")
    plt.ylabel("Relative spektrale Empfindlichkeit")
    plt.grid('on')

    # val=Finlayson(RGB_Bereinigt, RGB_Bereinigt, VlambdaMescopic, VlambdaMescopic, degree=[1,2,3,4], plotting=False)
    # plt.plot(np.arange(380,780),Y_CIE/np.max(Y_CIE),label="V(λ)[Approximiert]")
    # plt.plot(np.arange(380,780),VlambdaMescopic.T[0],"--",color="orange",label="V(λ)[Tatsächlich]")
    # plt.legend(loc="upper right")
    # plt.xlabel("Wellenlänge λ in nm")
    # plt.ylabel("Relative Empfindlichkeit")
    # plt.grid('on')
    # plt.savefig("PolyDeg4V.png")
    # plt.savefig("PolyDeg4V.pdf")
    # plt.show()
    # #

    plt.show()


def plot_RGB(RGB,Vmes):
    plt.plot(np.arange(380, 780), RGB.T[2] / np.max(RGB), "r", label="R(λ)")
    plt.plot(np.arange(380, 780), RGB.T[1] / np.max(RGB), "g", label="G(λ)")
    plt.plot(np.arange(380, 780), RGB.T[0] / np.max(RGB), "b", label="B(λ)")


    plt.plot()
    plt.legend(loc="upper right")
    plt.xlabel("Wellenlänge λ in nm")
    plt.ylabel("Relative Empfindlichkeit")
    plt.grid('on')

    plt.savefig("SpektralIR.png")


    plt.show()

def Quantitative_Auswertung(L_Pred, L_Mes):
    Abstand = abs(L_Pred - L_Mes)
    Mean_Abstand = np.mean(Abstand)
    Median_Abstand = np.median(Abstand)
    Prozent = Abstand * 100 / L_Mes
    Median_Prozent = np.median(Prozent)
    Mean_Prozent = np.mean(Prozent)

    print("Mean:", Mean_Abstand, "Median:", Median_Abstand, "Prozent Median:", Median_Prozent, "Prozent Mean:",
          Mean_Prozent, "Metrics R²:", metrics.r2_score(L_Pred, L_Mes))

    return


def CalculateVignetting(Image_Flat, Image_Dark):
    Image_Flat = OpenImage.ReadRawPi(Image_Flat)
    Image_Dark = OpenImage.ReadRawPi(Image_Dark)
    Image_Flat = OpenImage.Apply_Correction_DarkCurent(Image_Flat, Image_Dark)

    img_R = Image_Flat[0]
    img_G = Image_Flat[1]
    img_B = Image_Flat[2]

    # Bestimmt Mittelpunkt des Bildes
    Hohe = int((np.shape(img_R)[0]) / 2)
    Breite = int((np.shape(img_R)[1]) / 2)

    # Bestimmt Durchschnittlichen Mittelwert für Rechteck um Mittelpunkt
    Value_R = 0
    Value_G = 0
    Value_B = 0
    for h in range(Hohe - 20, Hohe + 20):
        for b in range(Breite - 20, Breite + 20):
            Value_R = Value_R + img_R[h, b]
            Value_G = Value_G + img_R[h, b]
            Value_B = Value_B + img_R[h, b]

    CorrectionFactor_R = int(Value_R / 1600)
    CorrectionFactor_G = int(Value_G / 1600)
    CorrectionFactor_B = int(Value_B / 1600)

    LUT_R = np.zeros((np.shape(img_R)[0], np.shape(img_R)[1]))
    LUT_G = np.zeros((np.shape(img_R)[0], np.shape(img_R)[1]))
    LUT_B = np.zeros((np.shape(img_R)[0], np.shape(img_R)[1]))

    for y in tqdm(range(0, np.shape(img_R)[0])):
        for x in range(0, np.shape(img_R)[1]):
            LUT_R[y][x] = img_R[y][x] / CorrectionFactor_R
            LUT_G[y][x] = img_G[y][x] / CorrectionFactor_G
            LUT_B[y][x] = img_B[y][x] / CorrectionFactor_B

    scipy.io.savemat("./Data/LUT_R.mat", {"LUTR": 1 / LUT_R})
    scipy.io.savemat("./Data/LUT_G.mat", {"LUTG": 1 / LUT_G})
    scipy.io.savemat("./Data/LUT_B.mat", {"LUTB": 1 / LUT_B})

    # plt.imshow(1/LUT_R)
    # plt.show()
    # plt.imshow(1/LUT_G)
    # plt.show()
    # plt.imshow(1/LUT_B)
    # plt.show()


"""Starten der API"""

if __name__ == '__main__':
    ################"""Pfadverzeichnis"""################################
    Pfad_Kameradaten_Excel = "D:/OneDrive - TU Darmstadt Fachgebiet Lichttechnik/Studienarbeit/Messungen/Messung 06.11/SpektraleEmpfindlichkeit/SpektralemitIR500000.xls"
    Pfad_Monochrommesstand = 'C:\\Users\johan\Documents\Masterarbeit_Leuchtdichte\LuxPy\Ausgabe Monochrommesstand\Messung_2020-10-28_08-13-46_mit_Parametersets_370_790_401_0_1\Messung_'
    Pfad_Testdaten = "D:/OneDrive - TU Darmstadt Fachgebiet Lichttechnik/Studienarbeit/Messungen/Messung 06.11/Auswertung/Kalibrierung/Messung_500000_2609.xlsx"
    Pfad_Testdaten2 = "D:/OneDrive - TU Darmstadt Fachgebiet Lichttechnik/Studienarbeit/Messungen/Messung 06.11/Auswertung/Kalibrierung/Messung_500000.xlsx"

    #####################################################################

    #####################################################################{}
    print("Starte Programm")
    ###############"""Eigentliche Programm"""###########################

    """Auswertung der Messdaten Monochrommesstand"""
    RGB_Bereinigt, RGB, SpektraleSumme, SpectralData, SPektraleSumme2 = Monochrommesstand(Pfad_Kameradaten_Excel,
                                                                                          Pfad_Monochrommesstand)

    """Laden der Initialisierten Daten des Monochrommesstands"""
    # RGB_Bereinigt, RGB, SpektraleSumme, SpectralData = Load_Presets()

    """Laden der Testdaten"""
    RGB_Test, L_Test = Auslesen_Kamerarohtdaten_Exel(Pfad_Testdaten)
    RGB_Test2, L_Test2 = Auslesen_Kamerarohtdaten_Exel(Pfad_Testdaten2)

    # x_train, x_test, y_train, y_test = train_test_split(RGB_Test, L_Test, test_size=0.4, random_state=50)

    Vmes, Vsco, CMF = GetVlambda()

    # Y_CIE = Calibration_Transformation_IEC(RGB_Test, L_Test)

    # Calibration_Transformation_Finlayson(RGB_Bereinigt, RGB_Test, L_Test, degree_finlay=3, degree_regression=3)
