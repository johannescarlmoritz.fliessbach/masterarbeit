from matplotlib import pyplot as plt
import numpy as np
import cv2
from xlwt import Workbook
import os
import tqdm

import io
def SNRFunc2(ss,PfadOrdner):
    Pfad1= "SNR1_"+str(ss)+".jpg"
    Pfad2= "SNR2_"+str(ss)+".jpg"

    image1 = ReadImage1(PfadOrdner+Pfad1)
    image2 = ReadImage1(PfadOrdner+Pfad2)

    imag=image1[1]-image2[1]
    #plt.imshow(imag)
    #plt.show()
    #dark=ReadImage1(f"C:/Users/johan/Desktop/Ziel/Dark_{ss}.jpg")
    for a in range(0, 3):
        for x in range(image1[a].shape[1]):
            for y in range(image1[a].shape[0]):
                if image1[a][y][x] -4100 < 0:
                    image1[a][y][x] == 0
                else:
                    image1[a][y][x] = image1[a][y][x] - 4100

    for a in range(0, 3):
        for x in range(image2[a].shape[1]):
            for y in range(image2[a].shape[0]):
                if image2[a][y][x] - 4100 < 0:
                    image2[a][y][x] == 0
                else:
                    image2[a][y][x] = image2[a][y][x] - 4100

   # if image1[a][y][x] - dark[a][y][x] < 0:
    return image1,image2



def SaveSnr2(image1,image2):
    wb = Workbook()
    sheet1 = wb.add_sheet("SNR")

    av1 = (image1[0] + image2[0]) / 2
    av2 = (image1[1] + image2[1]) / 2
    av3 = (image1[2] + image2[2]) / 2

    sheet1.write(0, 0, "Mean R")
    sheet1.write(0, 1, "Mean G")
    sheet1.write(0, 2, "Mean B")
    sheet1.write(0, 3, "temp R")
    sheet1.write(0, 4, "temp B")
    sheet1.write(0, 5, "temp C")

    for row in range(len(image1)):
        sheet1.write(row + 1, 0, Mean[row, 0])
        sheet1.write(row + 1, 1, Mean[row, 1])
        sheet1.write(row + 1, 2, Mean[row, 2])
        sheet1.write(row + 1, 3, SNR[row, 0])
        sheet1.write(row + 1, 4, SNR[row, 1])
        sheet1.write(row + 1, 5, SNR[row, 2])

    wb.save("SNR2.xls")


def SNRFunc(ss,PfadOrdner):

    Pfad1= "SNR1_"+str(ss)+".jpg"
    Pfad2= "SNR2_"+str(ss)+".jpg"

    image1 = ReadImage(PfadOrdner+Pfad1)
    image2 = ReadImage(PfadOrdner+Pfad2)
    dark=ReadImage(f"C:/Users/johan/Desktop/Ziel/Dark_{ss}.jpg")

    # image1[0]=  image1[0]-dark[0]
    # image1[1] = image1[1] - dark[1]
    # image1[2] = image1[2] - dark[2]
    #
    # image2[0] = image2[0] - dark[0]
    # image2[1] = image2[1] - dark[1]
    # image2[2] = image2[2] - dark[2]

    for a in range(0,3):
        for x in range(image1[a].shape[1]):
            for y in range(image1[a].shape[0]):
                if image1[a][y][x]-dark[a][y][x]<0:
                    image1[a][y][x]==0
                else:
                    image1[a][y][x]=image1[a][y][x]-dark[a][y][x]

    for a in range(0,3):
        for x in range(image2[a].shape[1]):
            for y in range(image2[a].shape[0]):
                if image2[a][y][x]-dark[a][y][x]<0:
                    image2[a][y][x]==0
                else:
                    image2[a][y][x]=image2[a][y][x]-dark[a][y][x]


    try:
        Mean = [np.mean((image1[0] + image2[0])/2), np.mean((image1[1] + image2[1])/2), np.mean((image1[2] + image2[2])/2)]
        Std = [np.std(np.abs(image1[0] - image2[0]))/np.sqrt(2), np.std(np.abs(image1[1] - image2[1]))/np.sqrt(2), np.std(np.abs(image1[2] - image2[2]))/np.sqrt(2)]
        print(np.std(np.abs(image1[0] - image2[0])))
        print(np.mean(image1[0]))
        print(np.mean(image2[0]))
        SNR= [(Mean[0])/Std[0],(Mean[1])/Std[1],(Mean[2])/Std[2]]

        # SNR = [np.mean(image1[0]) / np.std(image1[0]), np.mean(image1[1]) / np.std(image1[1]), np.mean(image1[1]) / np.std(image1[1])]
        # Mean= [np.mean(image1[0]),np.mean(image1[1]),np.mean(image1[2])]
    except:
        print("Fehler")
        SNR=0
        Mean=0

    return SNR,Mean

def SaveSNR(SNR,Mean):

    wb=Workbook()
    sheet1=wb.add_sheet("SNR")

    sheet1.write(0, 0, "Mean R")
    sheet1.write(0, 1, "Mean G")
    sheet1.write(0, 2, "Mean B")
    sheet1.write(0, 3, "SNR R")
    sheet1.write(0, 4, "SNR B")
    sheet1.write(0, 5, "SNR C")

    for row in range(len(SNR)):

        sheet1.write(row+1, 0, Mean[row,0])
        sheet1.write(row+1, 1, Mean[row,1])
        sheet1.write(row+1, 2, Mean[row,2])
        sheet1.write(row+1, 3, SNR[row,0])
        sheet1.write(row+1, 4, SNR[row,1])
        sheet1.write(row+1, 5, SNR[row,2])

    wb.save("SNR.xls")


from PIL import Image
def ReadImage1(Path):

    img = Path

    file = open(img, 'rb')
    img = io.BytesIO(file.read())

    ver = 3
    offset = {
        3: 18711040,
    }[ver]

    data = img.getvalue()[-offset:]
    assert data[:4] == 'BRCM'.encode("ascii")

    data = data[32768:]
    data = np.frombuffer(data, dtype=np.uint8)

    reshape, crop = {
        3: ((3056, 6112), (3040, 6084)),
    }[ver]
    data = data.reshape(reshape)[:crop[0], :crop[1]]

    data = data.astype(np.uint16)
    shape = data.shape
    unpacked_data = np.zeros((shape[0], int(shape[1] / 3 * 2)), dtype=np.uint16)
    unpacked_data[:, ::2] = (data[:, ::3] << 4) + (data[:, 2::3] & 0x0F)
    unpacked_data[:, 1::2] = (data[:, 1::3] << 4) + ((data[:, 2::3] >> 4) & 0x0F)
    data = unpacked_data

    data[1::2, 0::2]  # RED
    data[0::2, 0::2]  # GREEN
    data[1::2, 1::2]  # GREEN
    data[0::2, 1::2]  # BLUE

    # Red
    img_R = data[0::2, 0::2] << 4


###############################
#Define Crop
##############################
    # crop_y1=550
    # crop_y2=950
    # cropy_x1=800
    # cropy_x2=1200
    crop_y1=400
    crop_y2=950
    cropy_x1=500
    cropy_x2=1400

###############################

    img_R= img_R[crop_y1:crop_y2,cropy_x1:cropy_x2]
    # plt.imshow(img_R)
    # plt.show()

    # Blue
    img_B = data[1::2, 1::2] << 4

    # Green
    img_G1 = (data[1::2, 0::2] << 4)
    img_G2 = (data[0::2, 1::2] << 4)
    img_G12 = np.zeros((img_G1.shape[0], img_G2.shape[1]), dtype=np.uint16)



    for i in range(0, img_G1.shape[1]):
        for y in range(0, img_G1.shape[0]):
            img_G = (int(img_G1[y, i]) + int(img_G2[y, i])) / 2
            img_G12[y, i] = img_G


    img_G12 = img_G12[crop_y1:crop_y2, cropy_x1:cropy_x2]
    img_B = img_B[crop_y1:crop_y2, cropy_x1:cropy_x2]


    return [img_R, img_G12, img_B]




if __name__ == '__main__':


    Path1 = "C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Input/Me1__500000_ag1.jpg"
    Path2 = "C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Input/Me1__500000_ag1.jpg"
    Path2 = "C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Input/masterdark.jpg"

    PfadOrdner="F:/SNRHome/2.Versuch/SNRHome_2Versuch/"
    PfadOrdner="C:/Users/johan/Desktop/Ziel/"

    list1=["C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Input/Me1__500000_ag1.jpg","C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Input/Me1__500000_ag1.jpg"]
    list2=["C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Input/masterdark.jpg","C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Input/masterdark.jpg"]

    # list1=os.listdir(PfadOrdner)
    listenr=[100,200,300,400,500,600,700,800,900,1000,2000,3000,4000,5000,6000,7000,8000,9000,10000,20000,30000,40000,50000,60000,70000,80000,90000,100000,200000,300000,400000,500000,600000,800000]
    # #listenr=[0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100,200,300,400,500,600,700,800,900,1000,2000,3000,4000,5000,6000,7000,8000,9000,10000,20000,30000,40000,50000,60000,70000,80000,90000,100000,200000,300000,400000,500000,600000,700000,800000,900000]
    # #listenr=[10000, 20000, 30000, 40000, 50000, 60000, 70000, 80000, 90000, 100000, 200000, 300000, 400000, 500000, 600000,700000, 800000]
    #
    listenr=[50,100,200,400,800,1000,2000,4000,8000,20000,40000,80000,100000,200000,400000,800000,1000000,2000000,3000000,4000000]

    listenr=[2000,3000,4000,5000,6000,7000,8000,9000,10000,20000,30000,40000,50000,60000,70000,80000,90000,100000,200000,300000,400000,500000,600000,800000,900000,1000000,2000000,3000000,4000000]
    listenr=[70000,80000,90000,100000,200000,300000,400000,500000,600000,800000,900000,1000000,2000000,3000000,4000000]

    Mean=np.empty((65535,1))
    Stdandard=np.empty((65535,1))
    Faktor = np.ones((65535, 1))


    for ss in listenr:



        image1,image2=SNRFunc2(ss, PfadOrdner)


        av2 = (image1[1] + image2[1]) / 2
        print(np.min(av2))
        print(np.max(av2))


        av2=av2.reshape(-1,1)
        #av22=np.vstack((av22,av2))

        i1=image1[1].reshape(-1,1)
        i2 = image2[1].reshape(-1, 1)

        i3=np.hstack((i1,i2))
        #variance1=np.empty((len(i1),1))

        Test1=av2
        Test2= np.std((i1, i2),axis=0)
        #plt.scatter(av2, Test2)

        # for i in range(0,len(Stdandard)):
        #     Temp=0
        #     count=0
        #     for ii in range(0,len(Test2)):
        #         if Mean[i]==Test2[ii][0]:
        #             Temp=Test2[ii][0]+Temp
        #             conunt=count+1
        #         try:
        #             Stdandard[i]= (Stdandard[i]+(Temp/count))/2
        #         except:
        #             None



        for i in range(0,len(Test1),1):
            if Stdandard[int(Test1[i,0])]==0:
                #print(Test2[i,0])
                Stdandard[int(Test1[i,0]),0]=Test2[i,0]
                #print(Stdandard[int(Test1[i,0])])
                Mean[int(Test1[i,0]),0]=int(Test1[i,0])

            else:
                Stdandard[int(Test1[i,0]),0]= (Stdandard[int(Test1[i,0]),]+Test2[i,0])
                Faktor[int(Test1[i, 0]), 0] = Faktor[int(Test1[i, 0]), 0] + 1





    Stdandard = Stdandard / Faktor
    plt.scatter(Mean, Stdandard)
    plt.show()

    import pandas as pd
    import pandas as pd
    import matplotlib.pyplot as plt
    from matplotlib.ticker import (
        FormatStrFormatter,
        AutoMinorLocator,
        FuncFormatter,
    )

    import matplotlib.dates as mdates
    from matplotlib.dates import DateFormatter
    import xlsxwriter

    workbook = xlsxwriter.Workbook('arrays4.xlsx')
    worksheet = workbook.add_worksheet()

    row = 0

    for col, data in enumerate(Stdandard):
        worksheet.write_column(0, col, data)
        worksheet.write_column(1, col, Mean[col])

    workbook.close()

    # for i in range(int(np.min(av2)),int(np.max(av2)),10):
    #     count=0
    #     counter=0
    #     for ii in range(0,len(av2)):
    #         if av2[ii][0]==i:
    #             count=count+variance[ii][0]
    #             counter+=1
    #
    #         else:
    #             None
    #
    #     if count>0:
    #         plt.scatter(i,count/counter)



    # SNR=np.empty((0,3))
    # Mean=np.empty((0,3))
    # for i,ss in enumerate(listenr):
    #
    #     SNR_temp,Mean_temp= SNRFunc(ss,PfadOrdner)
    #
    #     SNR=np.vstack((SNR,np.array(SNR_temp)))
    #
    #     Mean=np.vstack((Mean,np.array(Mean_temp)))
    #
    #     print(SNR)
    #     print(Mean)
    #
    #
    # SaveSNR(SNR,Mean)

    # a="F:/Messungen1.2/SNR/"
    # print(os.listdir(a))
    # print(a)