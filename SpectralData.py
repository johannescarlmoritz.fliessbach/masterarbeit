import warnings  # supress warnings

warnings.filterwarnings('ignore')

from scipy.io import loadmat

import xlrd
import scipy
from OpenImage import *
import tqdm
from matplotlib import pyplot as plt
from xlwt import Workbook

import numpy as np

"""Öffnen der vom Monochrommesstand ausgegebenen Messdaten"""


def Auslesen_Spektrale_Daten_Monochrommesstand(Pfad):
    """
    Input: Verzeichnis (Pfad zu dem Ordner mit den .isd Datein
    Output: SpektralData (Enthält die spektralen Daten je Wellenlängenschritt
            SpektraleSumme (Enthält die Summe der gemessenen Strahlung je Wellenlängenschritt
    """

    SpectralData = np.empty((401, 400))
    SpektraleSumme = np.empty((400, 1))
    SpektraleSumme3dim = np.empty((400, 3))
    wv = 380

    """Öffnen der .isd Datein"""
    for x in range(0, 400):

        Name = str(Pfad + str(wv) + '_[370_790]_1.isd')
        TxtFile = open(Name, "r")
        Data = TxtFile.readlines()
        TxtFile.close()

        Wavelength = np.empty((400, 1))
        SpectralOutput = np.empty((400, 1))
        wv = wv + 1

        """Auslesen der .isd Datei"""
        index = 0
        Sum = 0
        for i in range(49, 449):
            """Auslesen der Wellenlänge"""

            Wavelength[index] = Data[i].split()[0]

            "Für Werte kleiner 0 setzte Wert auf 0"
            if float(Data[i].split()[1]) < 0:
                SpectralOutput[index] = np.array(0)
            else:
                SpectralOutput[index] = np.array(Data[i].split()[1])
                Sum = Sum + float(Data[i].split()[1])

            index = index + 1

        SpectralData[0] = Wavelength.T
        SpectralData[x + 1] = SpectralOutput.T
        SpektraleSumme[x] = Sum
        SpektraleSumme3dim[x] = [Sum, Sum, Sum]

    scipy.io.savemat("./Data/SpectralData.mat", {"SpectralData": SpectralData})
    scipy.io.savemat("./Data/SpektraleSumme.mat", {"SpectraleSumme": SpektraleSumme})

    return SpectralData, SpektraleSumme, SpektraleSumme3dim


def Auslesen_Kamerarohdaten_Monochrom_Excel(Pfad):
    """
    Input: Pfad (Pfad Exel-Tabelle)
    Output: Array mit RGB Werten
    """

    book = xlrd.open_workbook(Pfad)
    first_sheet = book.sheet_by_index(0)
    RGB = []
    nm = 380
    RGBB = np.empty((400, 3))
    for wv in range(2, first_sheet.nrows):
        R = first_sheet.cell(wv, 1).value
        G = first_sheet.cell(wv, 2).value
        B = first_sheet.cell(wv, 3).value

        RGBB[wv - 2][0] = R
        RGBB[wv - 2][1] = G
        RGBB[wv - 2][2] = B
        RGB.append([nm, R, G, B])
        nm = nm + 1
    scipy.io.savemat("./Data/RGB_Monochrom.mat", {"RGB": RGB})

    return RGB


def Auslesen_Kamerarohtdaten_Exel(Pfad):
    """
    Input: Pfad zu Exel-Tabelle
    Output: RGB Testdaten in Array
    """

    book = xlrd.open_workbook(Pfad)
    first_sheet = book.sheet_by_index(0)
    RGB_Test = np.empty((first_sheet.nrows - 2, 3))

    rows = first_sheet.nrows - 2
    L_Test = np.empty((rows, 3))
    index = 0

    for i in range(2, first_sheet.nrows):
        R = first_sheet.cell(i, 0).value
        G = first_sheet.cell(i, 1).value
        B = first_sheet.cell(i, 2).value
        L = first_sheet.cell(i, 3).value

        RGB_Test[index][0] = R
        RGB_Test[index][1] = G
        RGB_Test[index][2] = B

        L_Test[index] = [L, L, L]
        index = index + 1

    return RGB_Test, L_Test




def RGB_Bereinigen(RGB, Summe):
    """
    Input: RGB (RGB Werte die zu bereinigen sind)
            Summe (Spektrale Summe der Wellenlängen)
    Output: RGB_Bereinigt (Bereinigte RGB Werte)
    """

    RGB_Bereinigt = np.empty((400, 3))

    for wv in range(len(RGB)):

        Factor = 1
        Fac = 1
        if wv == 399:
            None
        else:
            weight = Summe[wv + 1]

        rot = (float(RGB[wv][1]) / weight * Factor) / Fac
        grun = (float(RGB[wv][2]) / weight * Factor) / Fac
        blau = (float(RGB[wv][3]) / weight * Factor) / Fac

        RGB_Bereinigt[wv] = [rot, grun, blau]

    scipy.io.savemat("./Data/RGB_Bereinigt.mat", {"RGB_Bereinigt": RGB_Bereinigt})
    return RGB_Bereinigt


def Load_Presets():
    RGB_Bereinigt = scipy.io.loadmat("./Data/RGB_Bereinigt.mat")["RGB_Bereinigt"]
    RGB = scipy.io.loadmat("./Data/RGB_Monochrom.mat")["RGB"]
    SpektraleSumme = scipy.io.loadmat("./Data/SpektraleSumme.mat")["SpectraleSumme"]
    SpectralData = scipy.io.loadmat("./Data/SpectralData.mat")["SpectralData"]

    return RGB_Bereinigt, RGB, SpektraleSumme, SpectralData


def Monochrommesstand(Pfad_Kameradaten_Excel, Pfad_Monochrommesstand):
    """Auslesen der Spektralen Daten des Monochrommesstands"""
    SpektraleDaten, SpektraleSumme, SpektraleSumme3dim = Auslesen_Spektrale_Daten_Monochrommesstand(
        Pfad_Monochrommesstand)
    """Auslesen der Rohdaten der Kamera von Messung Monochrommesstand"""
    RGB = Auslesen_Kamerarohdaten_Monochrom_Excel(Pfad_Kameradaten_Excel)
    """Korrektur der RGB Daten um Messdaten Monochrommesstand"""
    RGB_Bereinigt = RGB_Bereinigen(RGB, SpektraleSumme)

    return RGB_Bereinigt, RGB, SpektraleSumme, SpektraleDaten, SpektraleSumme3dim


"""Auswertung Spektrale Epmfindlichkeit"""
def select_Spektrale(img_R,img_G1,img_B):



    Blue=0
    Red=0
    Green=0
    Pixel = (int(400) - int(300)) * (int(300) - int(200))

    index=0
    for x in range(300,400):
        for y in range(200, 300):
            Blue = Blue + img_B[y, x]
            Red = Red + img_R[y, x]
            Green = Green + img_G1[y, x]
            index=index+1

    #print(Pixel,index )

    Mean_Red = round(Red / Pixel)
    Mean_Green = round(Green / Pixel)
    Mean_Blue = round(Blue / Pixel)

    return Mean_Red,Mean_Green,Mean_Blue


def OpenImages(PfadFile,PfadDark,ss):

    wb = Workbook()

    sheet1 = wb.add_sheet("SpektraleEmpfindlichkeit")

    sheet1.write(0, 0, "SpektraleEmpfindlichkeit")
    sheet1.write(1, 0, "Wellenlaenge")
    sheet1.write(1, 1, "Rot")
    sheet1.write(1, 2, "Grün")
    sheet1.write(1, 3, "Blau")

    xmin=800
    xmax=1400

    ymin=500
    ymax=900

    row=2
    for wv in range(380,780):
        Image_Path=str(PfadFile+str(wv)+"_"+str(ss)+".jpeg")
        #Image=ProcessImage(Image_Path,PfadDark)
        Image=ReadRawPi(Image_Path)


        if wv <= 390:
            plt.imshow(Image[0][ymin:ymax,xmin:xmax])
            plt.show()

        sheet1.write(row, 0, wv)
        sheet1.write(row, 1, round(np.mean(Image[0][ymin:ymax, xmin:xmax])))
        sheet1.write(row, 2, round(np.mean(Image[1][ymin:ymax, xmin:xmax])))
        sheet1.write(row, 3, round(np.mean(Image[2][ymin:ymax, xmin:xmax])))

        print("rot",np.mean(Image[0][ymin:ymax, xmin:xmax]))
        print("grun", np.mean(Image[1][ymin:ymax, xmin:xmax]))
        print("blau", np.mean(Image[2][ymin:ymax, xmin:xmax]))

        print("Aktuelle Wellenlaenge:", wv)
        row += 1

    wb.save("SpektralePhoto_500000_1.xls")


def Auslesen():

    OpenImages()

if __name__ == '__main__':
    ################"""Pfadverzeichnis"""################################
    Pfad_Kameradaten_Excel = "D:/OneDrive - TU Darmstadt Fachgebiet Lichttechnik/Studienarbeit/Messungen/Messung 06.11/SpektraleEmpfindlichkeit/SpektralemitIR500000.xls"
    Pfad_Monochrommesstand = 'C:\\Users\johan\Documents\Masterarbeit_Leuchtdichte\LuxPy\Ausgabe Monochrommesstand\Messung_2020-10-28_08-13-46_mit_Parametersets_370_790_401_0_1\Messung_'
    Pfad_Testdaten = "D:/OneDrive - TU Darmstadt Fachgebiet Lichttechnik/Studienarbeit/Messungen/Messung 06.11/Auswertung/Kalibrierung/Messung_500000_2609.xlsx"
    Pfad_Testdaten2 = "D:/OneDrive - TU Darmstadt Fachgebiet Lichttechnik/Studienarbeit/Messungen/Messung 06.11/Auswertung/Kalibrierung/Messung_500000.xlsx"

    """Auswertung der Messdaten Monochrommesstand"""
    # RGB_Bereinigt, RGB, SpektraleSumme, SpectralData,SpektraleSumme3dim = Monochrommesstand(Pfad_Kameradaten_Excel,Pfad_Monochrommesstand)

    """Laden der Initialisierten Daten des Monochrommesstands"""
    # RGB_Bereinigt, RGB, SpektraleSumme, SpectralData = Load_Presets()
    #
    # """Laden der Testdaten"""
    # RGB_Test, L_Test = Auslesen_Kamerarohtdaten_Exel(Pfad_Testdaten)
    # RGB_Test2, L_Test2 = Auslesen_Kamerarohtdaten_Exel(Pfad_Testdaten2)


    PfadImage="D:/OneDrive - TU Darmstadt Fachgebiet Lichttechnik/Studienarbeit/Messungen/Messung 06.11/SpektraleEmpfindlichkeit/Messerie_23.10/"
    PfadDark="C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Input/darkhalogen_500000.jpg"


    PfadImage="F:/SpektraleEmpfindlichkeit_1nm_photopicfilter_09022021_Versuch3/"

    PfadImage="D:/SpektraleEmpfindlichkeit_1nm_photopicfilter_09022021_Versuch3/"
    PfadDark="F:/Messungen1.2/Mit Filter/darkmitfilter1/darkmitfilter1_500000.jpg"
    Pfad="SpektraleEmpfind.xls"




    OpenImages(PfadImage,PfadDark,500000)

    #RGB_Bereinigt, RGB, SpektraleSumme, SpectralData, SpektraleSumme3dim = Monochrommesstand("SpektraleEmpfind.xls", Pfad_Monochrommesstand)