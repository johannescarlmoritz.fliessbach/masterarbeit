from tqdm import tqdm

from ReadRaw import *
import scipy

def CalculateVignetting(img_R,img_G,img_B):

    #Bestimmt Mittelpunkt des Bildes
    Hohe = int((np.shape(img_R)[0]) / 2)
    Breite = int((np.shape(img_R)[1]) / 2)

    #Bestimmt Durchschnittlichen Mittelwert für Rechteck um Mittelpunkt
    Value_R = 0
    Value_G = 0
    Value_B = 0
    for h in range(Hohe - 20, Hohe + 20):
        for b in range(Breite - 20, Breite + 20):
            Value_R = Value_R + img_R[h, b]
            Value_G = Value_G + img_R[h, b]
            Value_B = Value_B + img_R[h, b]


    CorrectionFactor_R = int(Value_R / 1600)
    CorrectionFactor_G = int(Value_G / 1600)
    CorrectionFactor_B = int(Value_B / 1600)

    LUT_R = np.zeros((np.shape(img_R)[0], np.shape(img_R)[1]))
    LUT_G = np.zeros((np.shape(img_R)[0], np.shape(img_R)[1]))
    LUT_B = np.zeros((np.shape(img_R)[0], np.shape(img_R)[1]))

    for y in tqdm(range(0, np.shape(img_R)[0])):
        for x in range(0, np.shape(img_R)[1]):
            LUT_R[y][x] = img_R[y][x] / CorrectionFactor_R
            LUT_G[y][x] = img_G[y][x] / CorrectionFactor_G
            LUT_B[y][x] = img_B[y][x] / CorrectionFactor_B

    scipy.io.savemat("./Data/LUT_R.mat", {"LUTR": 1/LUT_R})
    scipy.io.savemat("./Data/LUT_G.mat", {"LUTG": 1/LUT_G})
    scipy.io.savemat("./Data/LUT_B.mat", {"LUTB": 1/LUT_B})

def ApplyVignetting(img_R,img_G,img_B,LUT_R, LUT_G, LUT_B):

    imgR_v=  img_R*LUT_R
    imgG_v = img_G * LUT_G
    imgB_v = img_B * LUT_B

    # fig, ax = plt.subplots()
    # im = ax.imshow(imgR_v)
    # plt.title("R")
    # plt.show()
    #
    # fig, ax = plt.subplots()
    # im = ax.imshow(imgG_v)
    # plt.title("G")
    # plt.show()
    #
    # fig, ax = plt.subplots()
    # im = ax.imshow(imgB_v)
    # plt.title("B")
    # plt.show()
    #
    # fig, ax = plt.subplots()
    # im = ax.imshow(img_R)
    # plt.title("Original R")
    # plt.show()

    return imgR_v, imgG_v, imgB_v


def LoadLUT():

    LUT_R = scipy.io.loadmat("./Data/LUT_R.mat")["LUTR"]
    LUT_G = scipy.io.loadmat("./Data/LUT_G.mat")["LUTG"]
    LUT_B = scipy.io.loadmat("./Data/LUT_B.mat")["LUTB"]

    return LUT_R, LUT_G, LUT_B



if __name__ == '__main__':

    Bildname = "D:/OneDrive - TU Darmstadt Fachgebiet Lichttechnik/Studienarbeit/Messungen/Messung 23.09/Spektrale Empfindlichkeit/M2/402M2_200000.jpeg"
    Referenz = "D:/OneDrive - TU Darmstadt Fachgebiet Lichttechnik/Studienarbeit/Messungen/Messung 23.09/Spektrale Empfindlichkeit/M2/502M2_200000.jpeg"


    img_R, img_G, img_B = ReadRawPi(Bildname)
    Vig_R, Vig_G, Vig_B = ReadRawPi(Referenz)



    CalculateVignetting(Vig_R,Vig_G,Vig_B)
    LUT_R, LUT_G, LUT_B = LoadLUT()
    ApplyVignetting(img_R,img_G,img_B,LUT_R,LUT_G,LUT_B)

