from Application import *
import warnings  # supress warnings

from Application import *
from Calibration import *
from OpenFiles import *
from OpenImage import *
from SpectralData import *
from LuminanceImage import *
warnings.filterwarnings('ignore')
import os
import io
from xlwt import Workbook
from scipy.io import loadmat
import scipy
from tqdm import tqdm
from sklearn.model_selection import train_test_split
import numpy as np
from matplotlib import pyplot as plt



def Read_Trainigsdata(RGB_T,RGB_T_File,RGB_R,RGB_D,L_T,man=False):

    print("Start Auswertung Trainigsbilder")


    for i, Image in tqdm(enumerate(RGB_T)):
        print(Image)
        Img_Raw = ProcessImage(RGB_T_File+Image, RGB_D, exif=False, display=False)
        AnalogGain, Shutterspeed = Get_EXIF(RGB_T_File+Image)
        PixelColorChecker = Segmentation_CChecker(Img_Raw, RGB_R, display=True,manual=man)


        if i == 0:
            PixelValues = PixelColorChecker
        else:
            PixelValues = np.vstack((PixelValues, PixelColorChecker))

    R=PixelValues
    L=L_T
    PixelValues, L_norm = Normalize(PixelValues, L_T)
    #PixelValues = PixelValues*100 / (AnalogGain * Shutterspeed)
    Name = str(RGB_T[0][RGB_T[0].rfind("/")+1:RGB_T[0].find(".jpg")])
    Name = (str("./Data/" + Name + ".mat"))
    scipy.io.savemat(Name, {"RGB_Training": PixelValues})

    return PixelValues,L_norm,R,L

def Normalize(RGB,L):
    #Schneidet "zu große" und "zu kleine" Werte weg

    for i in range(-len(RGB)+1,1):
        #if np.min(RGB[-i])<300 or np.max(RGB[-i]>((65535-4100)*0.95)):
        if RGB[-i][1]<600 or np.max(RGB[-i]>((65535-4100)*0.95)):
            print(L[-i][0],np.min(RGB[-i]),np.max(RGB[-i]))
            RGB=np.delete(RGB,-i,0)
            L=np.delete(L,-i,0)

    return RGB,L

def Calibrate_All(RGB_Test, L_Test, RGB_Bereinigt,degree_finlay=3,degree_Regression=3,degree_iec=1):
    print("Start Calibration")
    Calibration_Transformation_IEC(RGB_Test, L_Test,degree_iec)

    Calibration_Transformation_Finlayson_Spectral(RGB_Bereinigt, RGB_Test, L_Test, degree_finlay, degree_Regression)

    Calibration_Transformation_CChecker(RGB_Test, L_Test, degree_regression=1)

    Calibration_Transformation_Finlayson(RGB_Test,L_Test,degree_finlay,degree_Regression)


def Apply_All(RGB, L_Ref=0):
    print("Start Application")

    print("Transformation: IEC")
    L_P = Apply_Transformation_IEC(RGB)
    if np.mean(L_Ref)!=0:
        Quantitative_Auswertung(L_P, L_Ref)

    #L_P2=None
    print("Transformation: Finlay")
    L_P2 = Apply_Transformation_FinlaysonSpectral(RGB)
    if np.mean(L_Ref)!=0:
        Quantitative_Auswertung(L_P2, L_Ref)

    print("Transformation: Finlay Checker")
    L_P3 = Apply_Transformation_Finlayson(RGB)
    if np.mean(L_Ref)!=0:
        Quantitative_Auswertung(L_P3, L_Ref)

    print("Transformation: CChecker")
    L_P4 = Apply_Transformatin_CChecker(RGB)
    if np.mean(L_Ref)!=0:
        Quantitative_Auswertung(L_P4, L_Ref)

   # L_P5 = None
    print("Transformation: Ensemble")
    L_P5 = Apply_Transformation_Ensemble(RGB)
    if np.mean(L_Ref)!=0:
        Quantitative_Auswertung(L_P5, L_Ref)

    return  L_P,L_P2,L_P3,L_P4,L_P5

def SaveLuminance(L_meas,L_P,L_P2,L_P3,L_P4,L_P5):

    wb=Workbook()
    sheet1=wb.add_sheet("Auswertung")

    sheet1.write(0, 0, "Measured Luminance")
    sheet1.write(0, 1, "IEC")
    sheet1.write(0, 2, "Wurzelpolynom")
    sheet1.write(0, 3, "Wurzelpolynom Checker")
    sheet1.write(0, 4, "Checker Linear")
    sheet1.write(0, 5, "Ensemble")

    for row in range(len(L_P)):

        sheet1.write(row+1, 0, L_meas[row,0])
        sheet1.write(row+1, 1, L_P[row,0])
        sheet1.write(row + 1, 2, L_P2[row,0])
        sheet1.write(row + 1, 3, L_P3[row,0])
        sheet1.write(row + 1, 4, L_P4[row,0])
        sheet1.write(row + 1, 5, L_P5[row, 0])


    wb.save("AuswertungLeuchtdichte.xls")

if __name__ == '__main__':

###########################################################################################



    Pfad_Kameradaten_Excel = "D:/OneDrive - TU Darmstadt Fachgebiet Lichttechnik/Studienarbeit/Messungen/Messung 06.11/SpektraleEmpfindlichkeit/SpektralemitIR500000.xls"
    Pfad_Monochrommesstand = 'C:\\Users\johan\Documents\Masterarbeit_Leuchtdichte\LuxPy\Ausgabe Monochrommesstand\Messung_2020-10-28_08-13-46_mit_Parametersets_370_790_401_0_1\Messung_'
    Pfad_Monochrommesstand = "D:/OneDrive - TU Darmstadt Fachgebiet Lichttechnik/Studienarbeit/Messungen/Messung 06.11/SpektraleEmpfindlichkeit/Ausgabe Monochrommesstand/Messung_2020-10-26_09-28-21_mit_Parametersets_370_790_401_0_1/Messung_"
    Pfad_Testdaten = "D:/OneDrive - TU Darmstadt Fachgebiet Lichttechnik/Studienarbeit/Messungen/Messung 06.11/Auswertung/Kalibrierung/Messung_500000_2609.xls"
    Pfad_Testdaten2 = "D:/OneDrive - TU Darmstadt Fachgebiet Lichttechnik/Studienarbeit/Messungen/Messung 06.11/Auswertung/Kalibrierung/Messung_500000.xls"
    Path_DarkFlat = "C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Input/masterdark.jpg"
    Testbild = "C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Input/Me1__500000_ag1.jpg"
    Pfad = "C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Bilder/Messung6_1_(2020-08-26_0155)400_300000.jpeg"
    PfadARW = "C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Bilder/DSC00028.ARW"
    Bildname = "D:/OneDrive - TU Darmstadt Fachgebiet Lichttechnik/Studienarbeit/Messungen/Messung 23.09/Spektrale Empfindlichkeit/M2/402M2_200000.jpeg"
    Referenz = "D:/OneDrive - TU Darmstadt Fachgebiet Lichttechnik/Studienarbeit/Messungen/Messung 23.09/Spektrale Empfindlichkeit/M2/502M2_200000.jpeg"

    PfadARW="D:\OneDrive - TU Darmstadt Fachgebiet Lichttechnik\Studienarbeit\Messungen\Messung 26.08\Sony Alpha\DSC00028.ARW"
    PfadARWJPG = "D:\OneDrive - TU Darmstadt Fachgebiet Lichttechnik\Studienarbeit\Messungen\Messung 26.08\Sony Alpha\DDSC00049.JPG"

######################################################################################################

    """Auswertung der Messdaten Monochrommesstand"""
    #Pfad_Kameradaten_Excel="SpektraleEmpfind.xls"
    #Pfad_Kameradaten_Excel = "C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/SpektralePhoto_500000_1_1.xls"
    RGB_Bereinigt, RGB, SpektraleSumme, SpectralData, SPektraleSumme2 = Monochrommesstand(Pfad_Kameradaten_Excel, Pfad_Monochrommesstand)
    """Laden der Initialisierten Daten des Monochrommesstands"""
    #RGB_Bereinigt, RGB, SpektraleSumme, SpectralData = Load_Presets()

    """Laden der Testdaten"""
    RGB_Test, L_Test = Auslesen_Kamerarohtdaten_Exel(Pfad_Testdaten)
    RGB_Test2, L_Test2 = Auslesen_Kamerarohtdaten_Exel(Pfad_Testdaten2)

    #RGB_T = scipy.io.loadmat("./Data/Me1__500000_ag1.mat")["RGB_Training"]
    L_T = Auslesen_Leuchtdichte_Exel("./Input/L_Training_2609.xls")
    #L_T = Auslesen_Leuchtdichte_Exel("./Input/L_Training_2609_ohne4.xls")

    Ref = "D:/OneDrive - TU Darmstadt Fachgebiet Lichttechnik/Studienarbeit/Messungen/Messung 23.09/Skript/Alle/Me1__500000_ag1.jpg"
    Ref2 = "D:/OneDrive - TU Darmstadt Fachgebiet Lichttechnik/Studienarbeit/Messungen/Messung 23.09/Skript/500000/Me1__500000_ag1.jpg"

    #Calibrate_All(RGB_Test,L_Test,RGB_Bereinigt)

    Vmes, Vsco, CMF = GetVlambda()
    Pfad_RGB_Monochrom="C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/SpektralePhoto_500000_1_1.xls"


    RGB_Bereinigt2, RGB, SpektraleSumme, SpectralData, SPektraleSumme2 = Monochrommesstand(Pfad_Kameradaten_Excel,Pfad_Monochrommesstand)
    RGB_Bereinigt, RGB, SpektraleSumme, SpectralData, SPektraleSumme2 = Monochrommesstand(Pfad_RGB_Monochrom,Pfad_Monochrommesstand)


    def Calibrate_Photopic():

        """Mit 5000"""
        RGB_T_File = "C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Input/Mit_5000/"
        RGB_R = "C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Input/Mit_5000/4mitfilter2_5000_15_500000.jpg"
        RGB_D = "C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Input/darkmitfilter1_500000.jpg"

        L_T = Auslesen_Leuchtdichte_Exel("./Input/L_Training_5000_mit.xls")

        RGB_T_Images = os.listdir(RGB_T_File)
        PixelValues5, L_norm, R, L = Read_Trainigsdata(RGB_T_Images, RGB_T_File, RGB_R, RGB_D, L_T,man=True)


        """Mit 2800"""
        RGB_T_File = "C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Input/Mit_2800/"
        RGB_D = "C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Input/darkmitfilter1_500000.jpg"

        L_T = Auslesen_Leuchtdichte_Exel("./Input/L_Training_2800_mit.xls")
        RGB_T_Images = os.listdir(RGB_T_File)

        PixelValues28, L_norm2, R, L = Read_Trainigsdata(RGB_T_Images, RGB_T_File, RGB_R, RGB_D, L_T,man=True)
        PixelValues3 = np.vstack((PixelValues5, PixelValues28))

        L_norm3 = np.vstack((L_norm, L_norm2))

        # x_train, y_train, x_test, y_test = train_test_split(PixelValues3, L_norm3, test_size=0.4, random_state=50)

        Calibrate_All(PixelValues3,L_norm3,RGB_Bereinigt)


    def Calibrate_Infrared():
        RGB_Test, L_Test = Auslesen_Kamerarohtdaten_Exel(Pfad_Testdaten)
        RGB_Test2, L_Test2 = Auslesen_Kamerarohtdaten_Exel(Pfad_Testdaten2)

        RGB_T_File = "C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Input/Ohne_5000/"
        RGB_R = "C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Input/Ohne_5000/5mitfilter_5000K_20_500000.jpg"
        RGB_D = "C:/Users/johan/Documents/Masterarbeit_Leuchtdichte/Skript_Final/Input/darkhalogen_500000.jpg"

        L_T = Auslesen_Leuchtdichte_Exel("./Input/L_Training_5000_ohne.xls")
        RGB_T_Images = os.listdir(RGB_T_File)
        AnalogGain, ShuterSpeed = Get_EXIF(RGB_R)


        PixelValues2, L_norm2, R, L = Read_Trainigsdata(RGB_T_Images, RGB_T_File, RGB_R, RGB_D, L_T, man=True)


        PixelInfra = np.vstack((RGB_Test, PixelValues2))


        LInfra = np.vstack((L_Test, L_norm2))


        Calibrate_All(PixelInfra, LInfra, RGB_Bereinigt2)




    #Calibrate_Infrared()
    #Apply_All(RGB_Test,L_Ref=L_Test)
    #Apply_All(RGB_Test, L_Ref=L_Test)


    Bild="2021-03-25_01-52-49_255_mitBrett_color_1000000_1"
    Image_Feld = "D:/Bildablage/"+Bild+".jpg"

    Ordner_Bilder="D:/Bildablage/"
    RGB_T_Images = os.listdir(Ordner_Bilder)

    Create_LuminanceImage(Image_Feld,Image_Feld,name=str(Bild)+"_",method="Ensemble",cmap=True,vmin=0.1,vmax=10,map="hot")
    #for Bilder in RGB_T_Images:
     #   Create_LuminanceImage(Ordner_Bilder+Bilder,Ordner_Bilder+Bilder,name=str(Bilder)+"_",method="All",cmap=True,vmin=0.1,vmax=20,map="hot")