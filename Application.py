from __future__ import division, unicode_literals

import warnings  # supress warnings

warnings.filterwarnings('ignore')

from scipy.io import loadmat
from SpectralData import *
import pickle

import numpy as np
from colour.utilities import (as_float_array)
import scipy
from Calibration import *

import warnings  # supress warnings

warnings.filterwarnings('ignore')

import colour
import luxpy as lx
import matplotlib.pyplot as plt
import numpy as np
import scipy
import xlrd
from colour import *
from scipy.io import loadmat
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import PolynomialFeatures
from SpectralData import *
from sklearn import metrics
import pickle


import numpy as np
from colour.algebra import least_square_mapping_MoorePenrose
from colour.utilities import (CaseInsensitiveMapping, as_float_array, as_int,
                              closest, filter_kwargs, tsplit, tstack)
import scipy



"""Apply IEC Transformation"""

def Apply_Transformation_IEC(RGB):

    Y_CIE= IEC_Transformation(RGB)
    Model= LoadLinearModel("LinModel_IEC.pkl")
    L_P= Model.predict(Y_CIE)

    return L_P


def LoadLinearModel(Savename):
    #savename="LinearModel.pkl"
    Savename="./Data/"+Savename
    with open(Savename, 'rb') as file:
        pickle_model = pickle.load(file)
    return pickle_model


def Apply_Transformation_FinlaysonSpectral(RGB):

    degree = scipy.io.loadmat("./Data/CCMFDegreeSpectral.mat")["CCMFDegree"]
    CCM = scipy.io.loadmat("./Data/CCMFinlaysonSpectral.mat")["CCMFinlayson"]

    """Aufteilen des RGB Arrays in Subarrays um Memory Overflow vorzubeugen"""

    if RGB.shape[0] % 2:

        Y_CIE = Apply_CCM_Finlayson(RGB, CCM, degree)
        Model = LoadLinearModel("ModelFinlaysonSpectral.pkl")
        L_P=Model.predict(Y_CIE)


    else:
        RGB_Split=np.split(RGB,2)

        RGB_List=[RGB_Split[0],RGB_Split[1]]
        L_P=np.empty((RGB.shape[0],3))

        i=0
        for a,RGB in enumerate(RGB_List):

            Y_CIE= Apply_CCM_Finlayson(RGB,CCM,degree)
            Model=LoadLinearModel("ModelFinlaysonSpectral.pkl")
            L_P[i:(RGB.shape[0]*(a+1))]= Model.predict(Y_CIE)
            i=i+RGB.shape[0]

    return L_P



def Apply_Transformation_Finlayson(RGB):

    degree = scipy.io.loadmat("./Data/CCMFDegree.mat")["CCMFDegree"]
    CCM = scipy.io.loadmat("./Data/CCMFinlayson.mat")["CCMFinlayson"]

    """Aufteilen des RGB Arrays in Subarrays um Memory Overflow vorzubeugen"""

    if RGB.shape[0] % 2:

        Y_CIE = Apply_CCM_Finlayson(RGB, CCM, degree)
        Model = LoadLinearModel("ModelFinlayson.pkl")
        L_P=Model.predict(Y_CIE)


    else:
        RGB_Split=np.split(RGB,2)

        RGB_List=[RGB_Split[0],RGB_Split[1]]
        L_P=np.empty((RGB.shape[0],3))

        i=0
        for a,RGB in enumerate(RGB_List):

            Y_CIE= Apply_CCM_Finlayson(RGB,CCM,degree)
            Model=LoadLinearModel("ModelFinlayson.pkl")
            L_P[i:(RGB.shape[0]*(a+1))]= Model.predict(Y_CIE)
            i=i+RGB.shape[0]

    return L_P


def Apply_Transformatin_CChecker(RGB):

    Model = LoadLinearModel("LinModel_CChecker.pkl")
    L_P = Model.predict(RGB)

    return L_P

def Apply_Transformation_Ensemble(RGB):

    Y_CC=Apply_Transformatin_CChecker(RGB)
    Y_Fin=Apply_Transformation_FinlaysonSpectral(RGB)
    Y_FinCC=Apply_Transformation_Finlayson(RGB)

    Y_Ensemble= (Y_CC+Y_FinCC+Y_Fin)/3

    return Y_Ensemble



def Quantitative_Auswertung(L_Pred, L_Mes):
    Abstand = abs(L_Pred - L_Mes)
    Mean_Abstand = np.mean(Abstand)
    Median_Abstand = np.median(Abstand)
    Prozent = Abstand * 100 / L_Mes
    Median_Prozent = np.median(Prozent)
    Mean_Prozent = np.mean(Prozent)


    print("Mean:", Mean_Abstand, "Median:", Median_Abstand, "Prozent Median:", Median_Prozent, "Prozent Mean:",
          Mean_Prozent, "Metrics R²:", metrics.r2_score(L_Pred, L_Mes))

    return

###########################################################################################################################
if __name__ == '__main__':

    Pfad_Testdaten = "D:/OneDrive - TU Darmstadt Fachgebiet Lichttechnik/Studienarbeit/Messungen/Messung 06.11/Auswertung/Kalibrierung/Messung_500000_2609.xlsx"
    Pfad_Testdaten2 = "D:/OneDrive - TU Darmstadt Fachgebiet Lichttechnik/Studienarbeit/Messungen/Messung 06.11/Auswertung/Kalibrierung/Messung_500000.xlsx"



    """Laden der Testdaten"""
    RGB_Test, L_Test = Auslesen_Kamerarohtdaten_Exel(Pfad_Testdaten)
    RGB_Test2, L_Test2 = Auslesen_Kamerarohtdaten_Exel(Pfad_Testdaten2)


    L_P=Apply_Transformation_IEC(RGB_Test2)
    Quantitative_Auswertung(L_P,L_Test2)

    L_P2=Apply_Transformation_FinlaysonSpectral(RGB_Test2)
    Quantitative_Auswertung(L_P2,L_Test2)

    L_P3=Apply_Transformatin_CChecker(RGB_Test2)
    Quantitative_Auswertung(L_P3,L_Test2)